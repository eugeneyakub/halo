#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math

class  Location:
    def __init__(self, halo_id, sector_id):
        self.halo_id = halo_id
        self.sector_id = sector_id

class Calculation:
    def __init__(self, arc, lon, lat):
        self.lon = math.radians(lon)
        self.lat = math.radians(lat)
        
        self.arc = arc                                                      #величина дуги
        self.r = 6371000                                                    #радиус земли
        self.earth_halo_lengh = 2 * math.pi * self.r
        
        print('earth circle length: ' + str(self.earth_halo_lengh))
        
        self.siblings = []
        
    def get_halo_id(self):
        self.part_count = math.ceil(self.earth_halo_lengh / (self.arc ))    #число частей
        print('part\'s count: ' + str(self.part_count))
        
        self.halo_count = math.ceil(self.part_count / 2.0)                    #число колец
        print('halo\'s count: ' + str(self.halo_count))
        
        self.delta =  math.pi  / self.halo_count                            #угол в радианах
        print('new delta: ' + str(self.delta))
        
        self.halo_id = math.floor((math.pi / 2 - self.lat) / self.delta)    #номер кольца
        print('halo id: ' + str(self.halo_id))
        print('')
    

            
    def is_exist_index(self, count, _id):
        if _id < 0 or _id > count - 1:
            return False
        return True
    
    def location_alredy_exist(self, location):
        if self.location.halo_id == location.halo_id and self.location.sector_id == location.sector_id:
            return True
        for existed_location in self.siblings:
            if existed_location.halo_id == location.halo_id and existed_location.sector_id == location.sector_id:
                return True
        return False
   
    def handleLatitude(self, lat):
        if lat >= math.pi / 2:
            lat = math.pi / 2  - 0.000001
        elif lat <= -math.pi / 2:
            lat = -math.pi / 2 + 0.000001
        return lat
    
    def handleLongitude(self, lon):
        if lon <= - math.pi:
            lon = math.pi
        elif lon >= math.pi:
            lon = math.pi;
        return lon
    
   
    def handle_index(self, count, index):
        if index > count - 1:
            index = index - count
        elif index < 0:
            index = index + count
        return index
   
    def get_for_point(self, halo_id, angle):
        halo_r = math.fabs(self.r * math.sin(angle ) )           #радиус кольца
        print('earth radius ' + str(self.r) + '\nhalo\'s radius: ' + str(halo_r))
        
        sector_count = math.ceil(2 * math.pi * halo_r / self.arc)               #число секторов в кольце
        print('sector\'s count: ' + str(sector_count))
        
        phi = 2 * math.pi  / sector_count
        print('new phi: ' + str(phi))

        sector_id = math.floor((math.pi - self.lon) / phi)        #номер сектора
        print('sector id: ' + str(sector_id))
        
        
        self.location = Location(halo_id, sector_id)
        
        handled_index = self.handle_index(sector_count, sector_id - 1)
        handled_location = Location(halo_id, handled_index)
        if not self.location_alredy_exist(handled_location):
            self.siblings.append(handled_location)
        
        handled_index = self.handle_index(sector_count, sector_id + 1)
        handled_location = Location(halo_id, handled_index)
        if not self.location_alredy_exist(handled_location):
            self.siblings.append(handled_location)
        
        
        
        self.lon_min = math.pi - self.handle_index(sector_count, (self.location.sector_id - 1)) * phi
        self.lon_max = math.pi - self.handle_index(sector_count, (self.location.sector_id + 2)) * phi
        print('lon_min: ' + str(self.lon_min) + ' lon_max: ' + str(self.lon_max))
   
   
    def get_for_sibling_halos(self, halo_id, angle):
        print('\nhalo_id ' + str(halo_id)) 
        halo_r = math.fabs(self.r * math.sin(angle ) )           #радиус кольца
        #print('earth radius ' + str(self.r) + '\nhalo\'s radius: ' + str(halo_r))
        
        sector_count = math.ceil(2 * math.pi * halo_r / self.arc)               #число секторов в кольце
        print('sector\'s count: ' + str(sector_count))
        
        phi = 2 * math.pi  / sector_count
        print('new phi: ' + str(phi))    
    
        sector_id = math.floor((math.pi - self.lon) / phi)        #номер сектора
        print('sector! id: ' + str(sector_id))
    
        sector_id_min = math.floor((math.pi - self.lon_min) / phi)        #номер сектора
        sector_id_max = math.ceil((math.pi - self.lon_max) / phi)        #номер сектора
        print('sector_id_min: ' + str(sector_id_min) + ' sector_id_max: ' + str(sector_id_max))
        
        
        if sector_id_min > sector_id_max:
            for i in range(sector_id_min, sector_count):
                handled_location = Location(halo_id, i)
                self.siblings.append(handled_location)
            for i in range(0, sector_id_max):
                handled_location = Location(halo_id, i)
                self.siblings.append(handled_location)
            self.siblings.append(Location(halo_id, sector_id_max))    
        elif sector_id_min < sector_id_max:
            for i in range(int(sector_id_min), int(sector_id_max)):
                handled_location = Location(halo_id, i)
                self.siblings.append(handled_location)
            self.siblings.append(Location(halo_id, sector_id_max)) 
        elif sector_id_min == sector_id_max:
            handled_location = Location(halo_id, sector_id_max)
            self.siblings.append(handled_location)
    def get_sector_and_siblings(self, halo_id, lat):
        halo_r = self.r * math.sin(math.pi / 2 - lat )            #радиус кольца
        print('earth radius ' + str(self.r) + '\nhalo\'s radius: ' + str(halo_r))
        
        sector_count = math.ceil(2 * math.pi * halo_r / self.arc)               #число секторов в кольце
        print('sector\'s count: ' + str(sector_count))
        
        phi = 2 * math.pi  / sector_count
        print('new phi: ' + str(phi))

        sector_id = math.floor((math.pi - self.lon) / phi)        #номер сектора
        print('sector id: ' + str(sector_id))

        if halo_id == self.halo_id:                                 #сектора не соседей
            self.location = Location(halo_id, sector_id)
            self.lon_min = self.handle_index(sector_count, (self.location.sector_id - 1)) * phi
            self.lon_max = self.handle_index(sector_count, (self.location.sector_id + 2)) * phi
        else:
            self.siblings.append(Location(halo_id, sector_id))
            
        if self.is_exist_index(sector_count, sector_id - 1) and not self.location_alredy_exist(Location(halo_id, sector_id - 1)):
            self.siblings.append(Location(halo_id, sector_id - 1))
        else:
            loc = Location(halo_id, sector_count - 1)
            if not self.location_alredy_exist(loc):
                self.siblings.append(loc)
                
        if self.is_exist_index(sector_count, sector_id + 1) and not self.location_alredy_exist(Location(halo_id, sector_id + 1)):
            self.siblings.append(Location(halo_id, sector_id + 1))
        else:
            loc = Location(halo_id, 0)
            if not self.location_alredy_exist(loc):
                self.siblings.append(loc)
        print('')
   
    def get_all(self):
        self.lon = self.handleLongitude(self.lon)
        self.lat = self.handleLatitude(self.lat)
        self.get_halo_id()
      
        self.get_for_point(self.halo_id,  self.delta * self.handle_halo_id(self.halo_id))
        if self.is_exist_index(self.halo_count, self.halo_id - 1 ):
            self.get_for_sibling_halos(self.halo_id - 1, self.delta * self.handle_halo_id(self.halo_id) - self.delta)
        if self.is_exist_index(self.halo_count, self.halo_id + 1 ):
            self.get_for_sibling_halos(self.halo_id + 1, self.delta * self.handle_halo_id(self.halo_id) + self.delta)


    def get_lons(self):
        pass

    def handle_halo_id(self, index):
        if index < math.floor(self.halo_count/2.0):
            index += 1
        return index
    

if __name__ == '__main__':
    arc = 50
    lon = 61.40
    lat = 55.16

    print('arc\'s length in meters: ' + str(arc) + ' \nlat lon: ' + str(math.radians(lat)) + ' ' + str(math.radians(lon)) )
    c = Calculation(arc, lon, lat)
    c.get_all()

    print('location: ')
    print('halo_id: ' + str(c.location.halo_id) + ' sector_id: ' + str(c.location.sector_id))
    print('siblings: ')
    for sibling in c.siblings:
        print('halo_id: ' + str(sibling.halo_id) + ' sector_id: ' + str(sibling.sector_id))
    
